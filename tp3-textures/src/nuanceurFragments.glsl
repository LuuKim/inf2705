#version 410

// Définition des paramètres des sources de lumière
layout (std140) uniform LightSourceParameters
{
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    vec4 position[3];      // dans le repère du monde
    float constantAttenuation;
    float linearAttenuation;
    float quadraticAttenuation;
} LightSource;

// Définition des paramètres des matériaux
layout (std140) uniform MaterialParameters
{
    vec4 emission;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float shininess;
} FrontMaterial;

// Définition des paramètres globaux du modèle de lumière
layout (std140) uniform LightModelParameters
{
    vec4 ambient;       // couleur ambiante
    bool localViewer;   // observateur local ou à l'infini?
    bool twoSide;       // éclairage sur les deux côtés ou un seul?
} LightModel;

layout (std140) uniform varsUnif
{
    // partie 1: illumination
    int typeIllumination;     // 0:Gouraud, 1:Phong
    bool utiliseBlinn;        // indique si on veut utiliser modèle spéculaire de Blinn ou Phong
    bool afficheNormales;     // indique si on utilise les normales comme couleurs (utile pour le débogage)
    // partie 2: texture
    int numTexCoul;           // numéro de la texture de couleurs appliquée
    int numTexNorm;           // numéro de la texture de normales appliquée
    bool utiliseCouleur;      // doit-on utiliser la couleur de base de l'objet en plus de celle de la texture?
    int afficheTexelFonce;    // un texel foncé doit-il être affiché 0:normalement, 1:mi-coloré, 2:transparent?
};

uniform sampler2D laTextureCoul;
uniform sampler2D laTextureNorm;

/////////////////////////////////////////////////////////////////

in Attribs {
    vec4 couleur;
    vec2 texCoord;
    vec3 normale, obsVec, lumiDir[3];
} AttribsIn;

out vec4 FragColor;

vec4 calculerReflexion( in vec3 L[3],  in vec3 N, in vec3 O )
{
    vec4 color = FrontMaterial.emission + FrontMaterial.ambient*LightModel.ambient;
    vec4 grisUniforme = vec4(0.7,0.7,0.7,1.0);

    for (int i = 0; i < 3; i++) {
        vec3 L = normalize( L[i] );
        color += FrontMaterial.ambient * LightSource.ambient;
        float NdotL = max( 0.0, dot( N, L ) );    
        if( NdotL > 0.0 ) {
            if(utiliseCouleur){ 
                color += LightSource.diffuse * FrontMaterial.diffuse * NdotL; 
            } else {
                color += grisUniforme * LightSource.diffuse * NdotL;
            }
            float NdotHV = max( 0.0, ( utiliseBlinn ) ? dot( normalize( L + O ), N ) : dot( reflect( -L, N ), O ) );
            color += FrontMaterial.specular * LightSource.specular * ( pow( NdotHV, FrontMaterial.shininess ) );
        }
    }

    return color;
}

void main( void )
{
    vec4 gris = vec4(0.5,0.5,0.5,0.5);
    vec4 color = AttribsIn.couleur;
    vec4 TexColor = texture(laTextureCoul, AttribsIn.texCoord);
    vec4 colorIllum = vec4 (0.0);
    vec3 L, N, O;
    N = normalize( gl_FrontFacing ? AttribsIn.normale : -AttribsIn.normale );

    if(numTexNorm != 0) {
        vec3 colorTexNorm = texture(laTextureNorm, AttribsIn.texCoord).rgb;
        N = normalize( normalize((colorTexNorm - 0.5) * 2) + N);
    }
    O = normalize( AttribsIn.obsVec );

    if (typeIllumination == 1) {
        color = calculerReflexion(AttribsIn.lumiDir, N, O);
        colorIllum = clamp( color, 0.0, 1.0 );
    } else if (typeIllumination == 0) {
        colorIllum = clamp( color, 0.0, 1.0);
    }

    if (numTexCoul != 0) {
        vec4 tempColorTexture = TexColor;
        if( TexColor[0] < 0.5 && TexColor[1] < 0.5 && TexColor[2] < 0.5 ) {

            if (afficheTexelFonce == 1) { //colorés
                tempColorTexture = gris;
            } else if (afficheTexelFonce == 2) { //transparents
                discard;
            }
            colorIllum *= tempColorTexture;
        }
    }
    FragColor = colorIllum;
    
}
