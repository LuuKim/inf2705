#version 410

// Définition des paramètres des sources de lumière
layout (std140) uniform LightSourceParameters //I
{
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    vec4 position[3];      // dans le repère du monde
    float constantAttenuation;
    float linearAttenuation;
    float quadraticAttenuation;
} LightSource;

// Définition des paramètres des matériaux
layout (std140) uniform MaterialParameters //K
{
    vec4 emission;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float shininess;
} FrontMaterial;

// Définition des paramètres globaux du modèle de lumière
layout (std140) uniform LightModelParameters
{
    vec4 ambient; //IMOD      // couleur ambiante
    bool localViewer;   // observateur local ou à l'infini?
    bool twoSide;       // éclairage sur les deux côtés ou un seul?
} LightModel;

layout (std140) uniform varsUnif
{
    // partie 1: illumination
    int typeIllumination;     // 0:Gouraud, 1:Phong
    bool utiliseBlinn;        // indique si on veut utiliser modèle spéculaire de Blinn ou Phong
    bool afficheNormales;     // indique si on utilise les normales comme couleurs (utile pour le débogage)
    // partie 2: texture
    int numTexCoul;           // numéro de la texture de couleurs appliquée
    int numTexNorm;           // numéro de la texture de normales appliquée
    bool utiliseCouleur;      // doit-on utiliser la couleur de base de l'objet en plus de celle de la texture?
    int afficheTexelFonce;    // un texel foncé doit-il être affiché 0:normalement, 1:mi-coloré, 2:transparent?
};

uniform mat4 matrModel;
uniform mat4 matrVisu;
uniform mat4 matrProj;
uniform mat3 matrNormale;

/////////////////////////////////////////////////////////////////

layout(location=0) in vec4 Vertex;
layout(location=2) in vec3 Normal;
layout(location=3) in vec4 Color;
layout(location=8) in vec4 TexCoord;

out Attribs {
    vec4 couleur;
    vec2 texCoord;
    vec3 normale, obsVec, lumiDir[3];
} AttribsOut;


vec4 calculerReflexion( in vec3 L[3],  in vec3 N, in vec3 O )
{
    vec4 color = FrontMaterial.emission + FrontMaterial.ambient*LightModel.ambient;
    vec4 grisUniforme = vec4(0.7,0.7,0.7,1.0);

    for (int i = 0; i < 3; i++) {
        vec3 L = normalize( L[i] );
        color += FrontMaterial.ambient * LightSource.ambient;
        float NdotL = max( 0.0, dot( N, L ) );
        if( NdotL > 0.0 ) {
            if(utiliseCouleur){
                color += LightSource.diffuse * FrontMaterial.diffuse * NdotL;
            } else {
                color += grisUniforme * LightSource.diffuse * NdotL;
            }
            float NdotHV = max( 0.0, ( utiliseBlinn ) ? dot( normalize( L + O ), N ) : dot( reflect( -L, N ), O ) );
            color += FrontMaterial.specular * LightSource.specular * ( pow( NdotHV, FrontMaterial.shininess ) );
        }
    }

    return color;
}

void main( void )
{
    AttribsOut.texCoord = TexCoord.st;

    if(typeIllumination == 0) {

        gl_Position = matrProj * matrVisu * matrModel * Vertex;
        AttribsOut.normale = matrNormale * Normal;
        vec3 pos = ( matrVisu * matrModel * Vertex ).xyz;
        AttribsOut.obsVec = ( LightModel.localViewer ? normalize(-pos) :
                                    vec3(0.0, 0.0, 1.0));
        vec3 L[3];
        for(int i = 0; i < 3; i++) {
            AttribsOut.lumiDir[i] = (matrVisu * LightSource.position[i]).xyz - pos;
            L[i] = AttribsOut.lumiDir[i];
        }
        vec3 normale = matrNormale * Normal;
        AttribsOut.normale = normale;
        vec3 N = normalize( normale );
        vec3 O = normalize( AttribsOut.obsVec );
        AttribsOut.couleur = calculerReflexion(L, N, O);

    } else {

        gl_Position = matrProj * matrVisu * matrModel * Vertex;
        AttribsOut.normale = matrNormale * Normal;
        vec3 pos = ( matrVisu * matrModel * Vertex ).xyz;
        AttribsOut.obsVec = ( LightModel.localViewer ? normalize(-pos) :
                                    vec3(0.0, 0.0, 1.0));
        vec3 L[3];
        for(int i = 0; i < 3; i++) {
            AttribsOut.lumiDir[i] = (matrVisu * LightSource.position[i]).xyz - pos;
            L[i] = AttribsOut.lumiDir[i];
        }
        vec3 normale = matrNormale * Normal;
        AttribsOut.normale = normale;
    }

}
