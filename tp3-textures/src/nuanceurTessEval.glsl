#version 410

layout(quads) in;

// Définition des paramètres des sources de lumière
layout (std140) uniform LightSourceParameters
{
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    vec4 position[3];      // dans le repère du monde
    float constantAttenuation;
    float linearAttenuation;
    float quadraticAttenuation;
} LightSource;

// Définition des paramètres des matériaux
layout (std140) uniform MaterialParameters
{
    vec4 emission;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float shininess;
} FrontMaterial;

// Définition des paramètres globaux du modèle de lumière
layout (std140) uniform LightModelParameters
{
    vec4 ambient;       // couleur ambiante
    bool localViewer;   // observateur local ou à l'infini?
    bool twoSide;       // éclairage sur les deux côtés ou un seul?
} LightModel;

layout (std140) uniform varsUnif
{
    // partie 1: illumination
    int typeIllumination;     // 0:Gouraud, 1:Phong
    bool utiliseBlinn;        // indique si on veut utiliser modèle spéculaire de Blinn ou Phong
    bool afficheNormales;     // indique si on utilise les normales comme couleurs (utile pour le débogage)
    // partie 2: texture
    int numTexCoul;           // numéro de la texture de couleurs appliquée
    int numTexNorm;           // numéro de la texture de normales appliquée
    bool utiliseCouleur;      // doit-on utiliser la couleur de base de l'objet en plus de celle de la texture?
    int afficheTexelFonce;    // un texel foncé doit-il être affiché 0:normalement, 1:mi-coloré, 2:transparent?
};

in Attribs {
    vec4 couleur;
    vec2 texCoord;
    vec3 normale, obsVec, lumiDir[3];
} AttribsIn[];

out Attribs {
    vec4 couleur;
    vec2 texCoord;
    vec3 normale, obsVec, lumiDir[3];
} AttribsOut;

vec4 calculerReflexion( in vec3 L[3],  in vec3 N, in vec3 O )
{
    vec4 color = FrontMaterial.emission + FrontMaterial.ambient*LightModel.ambient;
    vec4 grisUniforme = vec4(0.7,0.7,0.7,1.0);

    for (int i = 0; i < 3; i++) {
        vec3 L = normalize( L[i] );
        color += FrontMaterial.ambient * LightSource.ambient;
        float NdotL = max( 0.0, dot( N, L ) );
        if( NdotL > 0.0 ) {
            if(utiliseCouleur){
                color += LightSource.diffuse * FrontMaterial.diffuse * NdotL;
            } else {
                color += grisUniforme * LightSource.diffuse * NdotL;
            }
            float NdotHV = max( 0.0, ( utiliseBlinn ) ? dot( normalize( L + O ), N ) : dot( reflect( -L, N ), O ) );
            color += FrontMaterial.specular * LightSource.specular * ( pow( NdotHV, FrontMaterial.shininess ) );
        }
    }

    return color;
}

float interpole( float v0, float v1, float v2, float v3 )
{
    return mix(mix(v0, v1, gl_TessCoord.x), mix(v3, v2, gl_TessCoord.x), gl_TessCoord.y); 
}
vec2 interpole( vec2 v0, vec2 v1, vec2 v2 , vec2 v3 )
{
    return mix(mix(v0, v1, gl_TessCoord.x), mix(v3, v2, gl_TessCoord.x), gl_TessCoord.y); 
}
vec3 interpole( vec3 v0, vec3 v1, vec3 v2, vec3 v3  )
{
    return mix(mix(v0, v1, gl_TessCoord.x), mix(v3, v2, gl_TessCoord.x), gl_TessCoord.y); 
}
vec4 interpole( vec4 v0, vec4 v1, vec4 v2 , vec4 v3 )
{
    return mix(mix(v0, v1, gl_TessCoord.x), mix(v3, v2, gl_TessCoord.x), gl_TessCoord.y); 
}

void main()
{
    gl_Position = interpole( gl_in[0].gl_Position, gl_in[1].gl_Position, gl_in[3].gl_Position,  gl_in[2].gl_Position );

    AttribsOut.couleur = interpole( AttribsIn[0].couleur, AttribsIn[1].couleur, AttribsIn[3].couleur, AttribsIn[2].couleur );
    AttribsOut.texCoord = interpole( AttribsIn[0].texCoord, AttribsIn[1].texCoord, AttribsIn[3].texCoord, AttribsIn[2].texCoord );
    AttribsOut.obsVec = interpole( AttribsIn[0].obsVec, AttribsIn[1].obsVec, AttribsIn[3].obsVec, AttribsIn[2].obsVec );
    AttribsOut.normale = interpole( AttribsIn[0].normale, AttribsIn[1].normale, AttribsIn[3].normale, AttribsIn[2].normale );

    for (int i = 0 ; i < 3 ; i++) {
        AttribsOut.lumiDir[i] = interpole( AttribsIn[0].lumiDir[i], AttribsIn[1].lumiDir[i], AttribsIn[3].lumiDir[i], AttribsIn[2].lumiDir[i] );
    }

    vec3 N, O;

    N = normalize ( AttribsOut.normale);
    O = normalize(AttribsOut.obsVec);

    if(typeIllumination == 0) {
        AttribsOut.couleur = calculerReflexion(AttribsOut.lumiDir, N, O);
    }
}
