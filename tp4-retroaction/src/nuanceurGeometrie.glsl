#version 410

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

in Attribs {
    vec4 couleur;
    float tempsDeVieRestant;
    float sens; // du vol
} AttribsIn[];

out Attribs {
    vec4 couleur;
    vec2 texCoord;
} AttribsOut;

uniform mat4 matrProj;
uniform int texnumero;

void main()
{
    gl_Position = gl_in[0].gl_Position;
    gl_PointSize = gl_in[0].gl_PointSize;
    vec2 coins[4];
    coins[0] = vec2( -0.5,  0.5 );
    coins[1] = vec2( -0.5, -0.5 );
    coins[2] = vec2(  0.5,  0.5 );
    coins[3] = vec2(  0.5, -0.5 );
    for ( int i = 0 ; i < 4 ; ++i )
    {
        float fact = gl_in[0].gl_PointSize / 20;
        vec2 decalage = coins[i]; // on positionne successivement aux quatre coins
        vec4 pos = vec4( gl_in[0].gl_Position.xy + fact * decalage, gl_in[0].gl_Position.zw );

        gl_Position = matrProj * pos;    // on termine la transformation débutée dans le nuanceur de sommets

        AttribsOut.couleur = AttribsIn[0].couleur;
        AttribsOut.texCoord = coins[i] + vec2( 0.5, 0.5 ); // on utilise coins[] pour définir des coordonnées de texture

        if(texnumero == 2 || texnumero ==3) {
            float angle = 6.0*AttribsIn[0].tempsDeVieRestant;
            mat2 rotation = mat2( cos(angle), -sin(angle), sin(angle), cos(angle));
            AttribsOut.texCoord.x /= 16.0;
            AttribsOut.texCoord.x += floor(mod(18.0*AttribsIn[0].tempsDeVieRestant, 16.0))/16.0;
            AttribsOut.texCoord.x *= AttribsIn[0].sens;
        }

        EmitVertex();
    }
    
    
    
    // assigner la position du point
    // gl_Position = gl_in[0].gl_Position;

    // // assigner la taille des points (en pixels)
    // gl_PointSize = gl_in[0].gl_PointSize;

    // // assigner la couleur courante
    // AttribsOut.couleur = AttribsIn[0].couleur;

    // EmitVertex();
}
